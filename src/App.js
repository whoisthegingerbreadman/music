import './App.css';
import React from "react";
import PlayerVisualizer from "./components/PlayerVisualizer/PlayerVisualizer.js";
import moonSvg from "./assets/moon.svg";
import groundSvg from "./assets/ground.svg";

function App() {
  const title = "Horror Booth"
  const subTitle = "One Dimensional Cellular Automata Sounds";

  return (
    <div className="app">
      <div className="night-sky"></div>
      <div className="moon-wrapper">
        <img src={moonSvg} alt="Moon" className="moon-svg"/>
      </div>

      <header className="header">
        <h1 className="title">{title}</h1>
        <h2 className="sub-title">{subTitle}</h2>
      </header>

      <div className="main">
        <PlayerVisualizer/>
      </div>
      <img src={groundSvg} alt="Ground" className="ground-svg"/>

      <footer></footer>
    </div>
  );
}

export default App;
