import React, { Component } from "react";
import "./Controls.css";

class Controls extends Component {
    constructor(props){
        super(props);
        this.inputs = [
          { id: "rule_number", text: "Rule Number", min: 1, max: 255 },
          { id: "num_steps", text: "Number of Steps", min: 1, max: 100  },
          { id: "note_offset",text: "Note Offset", min: 40, max: 90  },
          { id: "note_distance", text: "Note Distance", min: 1, max: 11  },
          { id: "tempo", text: "Tempo", min: 70, max: 360  },
        ]
    }

    render() {
      let inputElems = this.inputs.map((value, index) => {
        return (
          <div className="input-wrapper" key={index}>
            <label htmlFor={value.id}>{value.text}</label>
            <input type="number"
                   id={value.id}
                   name={value.id}
                   onChange={this.props.updateSettings}
                   defaultValue={this.props.settings[value.id]}
                   min={value.min}
                   max={value.max}
            />
            <span className="value-hint">
              {value.min} - {value.max}
            </span>
          </div>
        )
      })

      return (
          <div className="controls">
            <form>{ inputElems }</form>
            <div className="buttons-wrap">
              <button onClick={this.props.play}>Play</button>
              <button className="stop" onClick={this.props.stop}>Stop</button>
            </div>
          </div>
      )
    }
}

export default Controls;