import React, { Component } from "react";
import CellularAutomaton1D from "../../modules/ca1d";
import {Player, PianoRollCanvasVisualizer} from "@magenta/music";
import Controls from "../Controls/Controls";
import "./PlayerVisualizer.css";

class PlayerVisualizer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            settings: {
                num_steps: 20,
                rule_number: 20,
                note_offset: 60,
                note_distance: 1,
                tempo:240
            },
            song: {
                notes: [
                    { pitch: 60, startTime: 0.0, endTime: 0.5 },
                    { pitch: 60, startTime: 0.5, endTime: 1.0 },
                    { pitch: 67, startTime: 1.0, endTime: 1.5 },
                    { pitch: 67, startTime: 1.5, endTime: 2.0 },
                    { pitch: 69, startTime: 2.0, endTime: 2.5 },
                    { pitch: 69, startTime: 2.5, endTime: 3.0 },
                    { pitch: 67, startTime: 3.0, endTime: 4.0 },
                    { pitch: 65, startTime: 4.0, endTime: 4.5 },
                    { pitch: 65, startTime: 4.5, endTime: 5.0 },
                    { pitch: 64, startTime: 5.0, endTime: 5.5 },
                    { pitch: 64, startTime: 5.5, endTime: 6.0 },
                    { pitch: 62, startTime: 6.0, endTime: 6.5 },
                    { pitch: 62, startTime: 6.5, endTime: 7.0 },
                    { pitch: 60, startTime: 7.0, endTime: 8.0 },
                ],
                totalTime: 8
            }
        }
        this.CA = null;
        this.player = null;

        this.updateSettings = this.updateSettings.bind(this);
        this.play = this.play.bind(this);
        this.stop = this.stop.bind(this);
        this.initialize_player = this.initialize_player.bind(this);
        console.log(this.state.song);
    }

    initialize_player() {
        let viz = new PianoRollCanvasVisualizer(
          this.state.song,
          document.getElementById("canvas"),
          {
              noteRGB: "92, 167, 158",
              activeNoteRGB: "241, 48, 48"
          }
        );

        this.player = new Player(false, {
            run: (note) => viz.redraw(note),
            stop: () => {
                console.log("done")
            }
        });
    }

    play() {
        if(this.player) {
            if(this.player.isPlaying()) {
                this.player.stop();
            }

            this.initialize_player()
            this.player.start(this.state.song);
        }
    }

    stop() {
        if (this.player && this.player.isPlaying())
            this.player.stop();
        this.initialize_player()
    }

    updateSettings(event) {
        let settings = {
            ...this.state.settings
        }
        if (event.target.id === "num_steps")
            settings.num_steps = parseInt(event.target.value);

        if (event.target.id == "rule_number")
            settings.rule_number = parseInt(event.target.value);

        if (event.target.id == "note_offset")
            settings.note_offset = parseInt(event.target.value);

        if (event.target.id == "note_distance")
            settings.note_distance = parseInt(event.target.value);

        if (event.target.id == "tempo")
            settings.tempo = parseInt(event.target.value);

        this.CA.Rule = settings.rule_number;
        this.CA._BaseNote = settings.note_offset;

        let song = this.CA.make_song(
          60 / settings.tempo,
          settings.num_steps,
          settings.note_distance
        );

        this.setState({ settings, song })
    }

    render() {
        return (
            <div className="player-visualizer">
                <Controls play={this.play}
                          stop={this.stop}
                          settings={this.state.settings}
                          updateSettings={event => this.updateSettings(event)}
                />
                <div className="canvas-wrap">
                    <canvas id="canvas"></canvas>
                </div>
            </div>
        )
    }

    componentDidMount() {
        this.CA = new CellularAutomaton1D();
        this.initialize_player();
    }
}

export default PlayerVisualizer;