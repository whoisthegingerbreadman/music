class CellularAutomaton1D {
    constructor() {
        this._CurrentState = [];
        this._NextState = [];
        this._NumberOfCells = 16;
        this._Rule = 30;
        this._BaseNote = 60;
        this._StateChangedEventHandlers = [];
        this._NumberOfCellsChangedEventHandlers = [];
        this.make_song.bind(this)
    }

    //---------------------------------------------------
    // PROPERTIES
    //---------------------------------------------------

    get StateChangedEventHandlers() { return this._StateChangedEventHandlers; }

    get NumberOfCellsChangedEventHandlers() { return this._NumberOfCellsChangedEventHandlers; }

    get CurrentState() { return this._CurrentState; }

    get NextState() { return this._NextState; }

    get Rule() { return this._Rule; }
    set Rule(Rule) { this._Rule = Rule; }

    get NumberOfCells() { return this._NumberOfCells; }
    set NumberOfCells(NumberOfCells) {
        this._NumberOfCells = NumberOfCells;
        this.FireNumberOfCellsChangedEvent();
    }

    //-------------------------------------------------------------------
    // METHODS
    //-------------------------------------------------------------------

    FireStateChangedEvent() {
        this._StateChangedEventHandlers.every(function (Handler) { Handler(); });
    }

    FireNumberOfCellsChangedEvent() {
        this._NumberOfCellsChangedEventHandlers.every(function (Handler) { Handler(); });
    }

    Randomize() {
        for (let i = 0; i < this._NumberOfCells; i++) {
            this._CurrentState[i] = parseInt(Math.random() * 2);
        }

        this.FireStateChangedEvent();
    }

    InitializeToCentre() {
        for (let i = 0; i < this._NumberOfCells; i++) {
            this._CurrentState[i] = 0;
        }

        this._CurrentState[Math.floor(this._NumberOfCells / 2)] = 1;

        this.FireStateChangedEvent();
    }

    CalculateNextState() {
        let PrevIndex;
        let NextIndex;
        let Neighbourhood;
        let RuleAsBinary = this._Rule.toString(2);

        // left pad binary to 8
        while (RuleAsBinary.length < 8)
            RuleAsBinary = "0" + RuleAsBinary;

        for (let i = 0; i < this._NumberOfCells; i++) {
            if (i == 0)
                PrevIndex = this._NumberOfCells - 1;
            else
                PrevIndex = i - 1;

            if (i == (this._NumberOfCells - 1))
                NextIndex = 0;
            else
                NextIndex = i + 1;

            Neighbourhood = this._CurrentState[PrevIndex].toString() + this._CurrentState[i].toString() + this._CurrentState[NextIndex].toString();

            switch (Neighbourhood) {
                case "111":
                    this._NextState[i] = RuleAsBinary[0];
                    break;
                case "110":
                    this._NextState[i] = RuleAsBinary[1];
                    break;
                case "101":
                    this._NextState[i] = RuleAsBinary[2];
                    break;
                case "100":
                    this._NextState[i] = RuleAsBinary[3];
                    break;
                case "011":
                    this._NextState[i] = RuleAsBinary[4];
                    break;
                case "010":
                    this._NextState[i] = RuleAsBinary[5];
                    break;
                case "001":
                    this._NextState[i] = RuleAsBinary[6];
                    break;
                case "000":
                    this._NextState[i] = RuleAsBinary[7];
                    break;
            }
        }

        for (let i = 0; i < this._NumberOfCells; i++) {
            this._CurrentState[i] = this._NextState[i];
        }

        this._NextState.length = 0;

        this.FireStateChangedEvent();
    }

    Iterate(Iterations) {
        for (let Iteration = 1; Iteration <= Iterations; Iteration++) {
            this.CalculateNextState();
        }
    }
    make_song(deltaTime, num_iterations,note_distance=1) {
        let curTime = 0;
        let song = { notes: [], totalTime: num_iterations * deltaTime };
        this.InitializeToCentre();
        for (let i = 0; i < num_iterations; i++) {


            let binInt = this._CurrentState;
            //console.log(binInt);
            let idx = 0;
            for (let x of binInt) {
                if (x == 1) {
                    song.notes.push({ pitch: this._BaseNote + idx*note_distance, startTime: curTime, endTime: curTime + deltaTime })
                }
                idx += 1;

            }
            curTime += deltaTime;
            this.CalculateNextState()
        }
        return song;
    }
}

export default CellularAutomaton1D