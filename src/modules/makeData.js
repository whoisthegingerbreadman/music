function makeBinaryCount(count) {
    let out = Array();

    let curTime = 0;
    let deltaTime = .25
    let song = { notes: [], totalTime: count * deltaTime }
    for (let i = 0; i < count; i++) {

        let elem = Array(8).fill("0");
        let binInt = dec2bin(i);
        let idx = 0;
        for (let x of binInt.split('').reverse()) {
            if (x == 1) {
                song.notes.push({ pitch: 60 + idx, startTime: curTime, endTime: curTime + deltaTime })
            }
            idx += 1;

        }
        out.push(elem);
        curTime += deltaTime;
    }
    return out, song;
}

export default makeBinaryCount;