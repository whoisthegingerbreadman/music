function dec2bin(dec) {
    return (dec >>> 0).toString(2);
}
function makeCARule(rule_number = 30) {
    let to_mapping = dec2bin(rule_number).split("").reverse();
    let idx = 0;
    let mapping = {}

    for (let i = 0; i < 8; i++) {
        let arr = Array(8).fill(0)
        let all_elems = dec2bin(i).split("").reverse()
        for (let j; j < all_elems.length; j++) {
            arr[j] = all_elems[j]
        }
        mapping[i] = arr
    }
    return mapping
}

